package ua.dp.levelup.wimm.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by e.krotov@hotmail.com on 18.11.2015.
 */
@Service
public class CustomSecurityFilter extends GenericFilterBean {

    private String username;
    private String password;
    private UsernamePasswordAuthenticationToken token;
   // private AuthenticationManager authenticationManager;


    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {

        logger.debug(this + "received authentication request from " + request.getRemoteHost() + " to " + request.getLocalName());

        if (request instanceof HttpServletRequest) {
           // if (((HttpServletRequest) request).getPathInfo().equals("/fill")) {chain.doFilter(request, response);}
            if (isAuthenticationRequired()) {
                username = request.getParameter("username");
                password = request.getParameter("password");
                token = new UsernamePasswordAuthenticationToken(username, password);
               // Authentication authentication = authenticationManager.authenticate(token);
                SecurityContextHolder.getContext().setAuthentication(token);
//                // extract token from header
//                OEWebToken token = extractToken(request);
//
                // dump token into security context (for authentication-provider to pick up)
//                SecurityContextHolder.getContext().setAuthentication(token);
            } else {
                logger.debug("session already contained valid Authentication - not checking again");
            }

        }

        chain.doFilter(request, response);
    }

    private boolean isAuthenticationRequired() {
        // apparently filters have to check this themselves.  So make sure they have a proper AuthenticatedAccount in their session.
        Authentication existingAuth = SecurityContextHolder.getContext().getAuthentication();
        if ((existingAuth == null) || !existingAuth.isAuthenticated()) {
            return true;
        }

        if (!existingAuth.isAuthenticated()) {
            return true;
        }

        // current session already authenticated
        return false;
    }
}
