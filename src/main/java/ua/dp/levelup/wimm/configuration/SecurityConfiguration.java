package ua.dp.levelup.wimm.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

/**
 * Created by y.voytovich on 04.09.2015.
 */

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages={"ua.dp.levelup.wimm.configuration"})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomSecurityFilter customSecurityFilter;

    @Autowired
    DataSource dataSource;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select username,password,enabled from user where username = ?")
                .authoritiesByUsernameQuery("select username,role as authority from user where username = ?");

    }


    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }

    @Bean
    public SavedRequestAwareAuthenticationSuccessHandler
    savedRequestAwareAuthenticationSuccessHandler() {

        SavedRequestAwareAuthenticationSuccessHandler auth
                = new SavedRequestAwareAuthenticationSuccessHandler();
        auth.setTargetUrlParameter("targetUrl");
        return auth;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/fill/user")
                .antMatchers("/api/user/add/**")
                .antMatchers("/");
    }


@Override
protected void configure(HttpSecurity http) throws Exception {
    http
            .csrf().disable()
            .authorizeRequests()
           // .antMatchers("/").permitAll()
           // .antMatchers("/login").permitAll()
           // .antMatchers("/fill").permitAll()
            .antMatchers("/api/report/**").authenticated()
            .antMatchers(HttpMethod.GET, "/fill/expense").hasRole("ADMIN")
            //.antMatchers(HttpMethod.GET, "/fill/expense2").hasRole("ADMIN")
            .antMatchers(HttpMethod.POST, "/api/**").hasRole("ADMIN")
            .antMatchers(HttpMethod.GET, "/api/**").hasRole("ADMIN")
            //.antMatchers(HttpMethod.POST, "/api/payment/notification").hasRole("ADMIN")
            .antMatchers(HttpMethod.PUT, "/api/**").hasRole("ADMIN")
            .antMatchers(HttpMethod.DELETE, "/api/**").hasRole("ADMIN")
            .antMatchers("/user").authenticated()

//            .and().formLogin()
//            .successHandler(savedRequestAwareAuthenticationSuccessHandler())
//            .loginPage("/login")
//            .usernameParameter("username").passwordParameter("password")
//            .and().rememberMe().tokenRepository(persistentTokenRepository()).tokenValiditySeconds(1209600)
//            .and().csrf()
//            .and().exceptionHandling().accessDeniedPage("/Access_Denied");

    .and().addFilterBefore(customSecurityFilter, UsernamePasswordAuthenticationFilter.class);




}
}
