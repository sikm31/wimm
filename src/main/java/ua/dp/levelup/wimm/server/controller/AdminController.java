package ua.dp.levelup.wimm.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by y.voytovich on 07.08.2015.
 */
@Controller
@RequestMapping("/api/admin")
public class AdminController {

    Logger logger = LoggerFactory.getLogger(AdminController.class);

    @RequestMapping(method = RequestMethod.GET)
    public String getAdminPage() {
        return "admin/admin";
    }


    @RequestMapping(value = "/expensemanage",method = RequestMethod.GET)
    public String getProductPage() {
        return "redirect:/admin/expensemanagment.html";
    }

    @RequestMapping(value = "/usermanage",method = RequestMethod.GET)
    public String getUserPage() {
        return "redirect:/admin/usermanagement.html";
    }

}
