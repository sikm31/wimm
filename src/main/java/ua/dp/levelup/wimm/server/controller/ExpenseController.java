package ua.dp.levelup.wimm.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.model.ExpenseNotification;
import ua.dp.levelup.wimm.server.service.ExpenseService;

import javax.management.Notification;
import java.io.Serializable;
import java.util.List;

/**
 * Created by yuri on 29.08.2015.
 */
@RestController
@RequestMapping("/api/payment")
public class ExpenseController implements IRestController<Expense> {

    @Autowired
    private ExpenseService expenseService;


    @Override
    @RequestMapping(method = RequestMethod.GET)
    public List<Expense> getAll() {
        return expenseService.getAll();
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Expense get(@PathVariable("id") Serializable key) {
        return expenseService.getExpense(Integer.valueOf((String) key));
    }

    @Override
    public void put(Expense type) {

    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public void update(Expense type) {

        expenseService.editExpense(type);
    }

    @Override
    public void delete(Expense type) {

    }

    @Override
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public boolean deleteById(@PathVariable("id") Serializable key) {
        expenseService.deleteExpense(Integer.valueOf((String) key));
        return true;
    }




//    @RequestMapping("/fill")
//    public void fillDB() {
//
//        expenseService.addExpense(
//                new Expense(
//                        "url",
//                        "title",
//                        "comment",
//                        "cost",
//                        "costType",
//                        new ArrayList<Payment>(Arrays.asList(
//                                new Payment("sadfad", 1d),
//                                new Payment("asdffdsasd", 2d))), new Repeat("single")));
//    }


}
