package ua.dp.levelup.wimm.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.model.Payment;
import ua.dp.levelup.wimm.server.model.Repeat;
import ua.dp.levelup.wimm.server.model.User;
import ua.dp.levelup.wimm.server.service.ExpenseService;
import ua.dp.levelup.wimm.server.service.UserService;
import ua.dp.levelup.wimm.server.utils.SpringSecurityUtil;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by y.voytovich on 18.09.2015.
 * Use only for testing
 */
    @RestController
    @RequestMapping("/fill")
    public class FillController {

    @Autowired
    private UserService userService;

    @Autowired
    private ExpenseService expenseService;

    Calendar calendar = Calendar.getInstance();

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public void fillUser() {
        //User user = new User();
        if (userService.getUserByUserName("evgen")==null) {

        userService.addUser(new User("evgen","123",true,"ROLE_ADMIN"));
        userService.addUser(new User("yuri","123",true,"ROLE_ADMIN"));
        userService.addUser(new User("dan","123",true,"ROLE_ADMIN"));
        userService.addUser(new User("mkyoung","123",true,"ROLE_USER"));
        }
    }

    @RequestMapping(value = "/expense", method = RequestMethod.GET)
    public void fillExpense2(HttpServletRequest request) {

        Integer userId;
        if (request.getUserPrincipal() != null) {
            userId = userService.getUserByUserName(request.getUserPrincipal().getName()).getId();
        } else userId = userService.getUserByUserName(SpringSecurityUtil.getUsername()).getId();
        for (int i = 1; i < 10; i++) {
            expenseService.addExpense(
                    new Expense(
                            "url",
                            "title",
                            "comment",
                            10,
                            "UAH",
                            new ArrayList<Payment>(Arrays.asList(
                                    new Payment("testPayment", i*2, calendar.getTime()),
                                    new Payment("testPayment", i*3, calendar.getTime()),
                                    new Payment("testPayment", i*4, calendar.getTime()))),
                            userId,
                            Calendar.getInstance().getTime(),
                            Calendar.getInstance().getTime(),
                            new Repeat(null)));
        }
    }
}
