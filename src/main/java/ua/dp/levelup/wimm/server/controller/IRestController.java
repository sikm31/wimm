package ua.dp.levelup.wimm.server.controller;

import ua.dp.levelup.wimm.server.model.ExpenseNotification;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dan on 30.08.2015.
 */
public interface IRestController<T> {

    List<T> getAll();

    T get(Serializable key);


    void put(T type);

    void update(T type);

    void delete(T type);

    boolean deleteById(Serializable key);

}
