package ua.dp.levelup.wimm.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.dp.levelup.wimm.server.model.ExpenseNotification;
import ua.dp.levelup.wimm.server.service.NotificationService;

import java.util.List;

/**
 * Created by Evgen on 20.12.2015.
 */

@RestController
@RequestMapping("/notification")
public class NotificationController  {

     @Autowired
     private NotificationService notificationService;

    @RequestMapping(method = RequestMethod.GET)
    public List<ExpenseNotification> getNotification() {
        return notificationService.getNotification() ;
    }

}
