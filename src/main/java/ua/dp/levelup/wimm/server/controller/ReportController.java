package ua.dp.levelup.wimm.server.controller;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.token.Token;
import org.springframework.web.bind.annotation.*;
import ua.dp.levelup.wimm.server.dao.ExpenseDao;
import ua.dp.levelup.wimm.server.dao.ExpenseDaoImpl;
import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.service.ExpenseService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Евгений on 21.10.2015.
 */
@RestController
@RequestMapping("/api/report")
public class ReportController {

    @Autowired
    private ExpenseService expenseService;


    /* Example FORMAT Param /api/report?startdate=2015/10/21&enddate=2015/10/21 */
    @RequestMapping(method = RequestMethod.GET)
    public List<Expense> getAllForPeriod(
            @RequestParam(value = "startdate", required = false) Date startDate,
            @RequestParam(value = "enddate", required = false) Date endDate) {
        List<Expense> list;
        if (startDate == null && endDate == null) {
            list = expenseService.getAll();
        } else {
            //list = expenseService.getAllForPeriod(startDate, endDate);
            list = expenseService.getExpenseAndPaymentForPeriod(startDate, endDate);
        }

        return list;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Expense get(@PathVariable("id") Serializable key) {
        return expenseService.getExpense(Integer.valueOf((String) key));
    }
//
//    @RequestMapping(value = "/token", method = RequestMethod.GET)
//    public String get() {
//        return sessionFactory.getCurrentSession().getEntityName(Token.class);
//    }
}
