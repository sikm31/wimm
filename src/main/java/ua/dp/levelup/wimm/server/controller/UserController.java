package ua.dp.levelup.wimm.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.web.bind.annotation.*;

import ua.dp.levelup.wimm.server.model.User;
import ua.dp.levelup.wimm.server.service.ExpenseService;
import ua.dp.levelup.wimm.server.service.UserService;
import ua.dp.levelup.wimm.server.utils.SpringSecurityUtil;

import java.util.List;


/**
 * Created by e.krotov@hotmail.com on 17.09.2015.
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ExpenseService expenseService;

//    @RequestMapping(method = RequestMethod.GET)
//    public User get() {
//        return  userService.getUserByUserName(SpringSecurityUtil.getUsername());
//    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() {
        List<User> users = userService.getAll();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    @RequestMapping(/*value = "/create",*/ method = RequestMethod.POST)
    public void createUser(@RequestBody User user) {
        System.out.println("Creating User " + user.getUsername());
        userService.addUser(user);
       // userService.addUser(user);
       // return new ResponseEntity<Void>(HttpStatus.CREATED);
    }


//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public void update(User user) {
//        userService.editUser(user);
//    }
//
//    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
//    public void delete(User user) {
//        userService.deleteUser(user.getId());
//    }
    //-------------------Retrieve Single User--------------------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") Integer id) {
        System.out.println("Fetching User with id " + id);
        User user = userService.getUserById(id);
        if (user == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }




    //------------------- Update a User --------------------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User user) {
        userService.editUser(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }



    //------------------- Delete a User --------------------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") Integer id) {
        System.out.println("Fetching & Deleting User with id " + id);

//        User user = userService.getUserById(id);
//        if (user == null) {
//            System.out.println("Unable to delete. User with id " + id + " not found");
//            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
//        }

        userService.deleteUser(id);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }

}
