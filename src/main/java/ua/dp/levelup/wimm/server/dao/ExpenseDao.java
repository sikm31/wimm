package ua.dp.levelup.wimm.server.dao;

import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.model.ExpenseNotification;
import ua.dp.levelup.wimm.server.model.Payment;

import java.util.Date;
import java.util.List;

/**
 * Created by yuri on 29.08.2015.
 */
public interface ExpenseDao {
    public void addExpense(Expense expense);
    public void deleteExpense(Integer id);
    public void editExpense(Expense expense);
    public List<Expense> getAll();
    public List<Expense> getAllForPeriod(Date startPoint, Date endPoint);
    public List<Expense> getExpenseAndPaymentForPeriod(Date startPoint, Date endPoint);
    public List<Expense> getAllExpenseWithoutPayment();/* method return all Expense current user without Payment */
    public Expense getExpense(Integer id);

}
