package ua.dp.levelup.wimm.server.dao;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.wimm.server.model.*;
import ua.dp.levelup.wimm.server.service.UserService;
import ua.dp.levelup.wimm.server.utils.SpringSecurityUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by yuri on 29.08.2015.
 */

@Repository
@Transactional
public class ExpenseDaoImpl implements ExpenseDao {

    Logger logger = LoggerFactory.getLogger(ExpenseDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private PaymentDao paymentDao;

    @Autowired
    private UserService userService;

    @Override
    public void addExpense(Expense expense) {
        //expense.setUser_id(SpringSecurityUtil.getUsername());
        sessionFactory.getCurrentSession().persist(expense);
    }

    @Override
    public void deleteExpense(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Expense expense = getExpense(id);
        session.delete(expense);
    }

    @Override
    public void editExpense(Expense expense) {
        Session session = sessionFactory.getCurrentSession();
        Expense expense2 = getExpense(expense.getId());
        session.save(expense2);

    }

    @Override
    public List<Expense> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Expense WHERE user_id = :userId");
        query.setParameter("userId", userService.getUserByUserName(SpringSecurityUtil.getUsername()).getId() );
        return query.list();
    }

    @Override
    public Expense getExpense(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Expense expense = (Expense) session.get(Expense.class, id);
        return expense;
    }

    /* method return all Expense for period with ALL PAYMENTS for this Expense (Payment is not filtering) */
    @Override
    public List<Expense> getAllForPeriod(Date startDate, Date endDate) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery  ("FROM Expense " +
                                            "WHERE user_id = :userId " +
                                            "AND startDate  >= :startdate " +
                                            "AND endDate <= :enddate"); // учитывать что при вводе параметра endDate типа 2015/10/29 будут показаты експенсы до 2015/10/28 включительно
        query.setParameter("userId", userService.getUserByUserName(SpringSecurityUtil.getUsername()).getId());
        query.setParameter("startdate", startDate);
        query.setParameter("enddate", endDate);
        return query.list();

    }
    /* method return all Expense with Payment for period (Payment only for period) */
    @Override
    public List<Expense> getExpenseAndPaymentForPeriod(Date startDate, Date endDate) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT id, " +
                "comment, " +
                "cost, " +
                "currency, " +
                "image, " +
//                "type, " +
                "title, " +
                "user_id, " +
                "endDate, " +
//                "day, " +
                "startDate " +
                "FROM Expense " +
                "WHERE user_id = :userId " +
                "AND startDate  >= :startdate " +
                "AND endDate <= :enddate");
        query.setParameter("userId", userService.getUserByUserName(SpringSecurityUtil.getUsername()).getId());
        query.setParameter("startdate", startDate);
        query.setParameter("enddate", endDate);
        List<Expense> expenseList = new ArrayList<Expense>();
        List<Object[]> rows = query.list();
        for(Object[] row : rows){
            Expense expense = new Expense();
            expense.setId(Integer.parseInt(row[0].toString()));
            expense.setComment(row[1].toString());
            expense.setCost(Integer.parseInt(row[2].toString()));
            expense.setCurrency(row[3].toString());
            expense.setImage(row[4].toString());
            expense.setTitle(row[5].toString());
            expense.setUser_id(Integer.parseInt(row[6].toString()));
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            try {
                expense.setEndDate(df.parse(row[7].toString()));
                expense.setStartDate(df.parse(row[8].toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            expenseList.add(expense);
        }
        for (Expense expense : expenseList) {
            Query queryPayment = session.createQuery("FROM Payment " +
                    "WHERE expense_id = :expenseid " +
                    "AND date >= :startdate " +
                    "AND date <= :enddate");
            queryPayment.setParameter("expenseid", expense.getId());
            queryPayment.setParameter("startdate", startDate);
            queryPayment.setParameter("enddate", endDate);
            expense.getHistory().addAll(queryPayment.list());
        }

        return expenseList;

    }
    /* method return all Expense current user without Payment */
    @Override
    public List<Expense> getAllExpenseWithoutPayment() {


        Session session = sessionFactory.getCurrentSession();
        Query queryGetExpense = session.createSQLQuery(
                "SELECT " +
                        "id," +         //0
                        "comment," +    //1
                        "title," +      //2
                        "cost," +
                        "currency," +
                        "startDate," +  //5
                        "endDate," +    //6
                        "type," +
                        "user_id " +    //8
                        "FROM expense " +
                        "WHERE user_id =:userId");

        System.out.println("GGGGG"+userService.getUserByUserName(SpringSecurityUtil.getUsername()).getId());

        queryGetExpense.setParameter("userId", userService.getUserByUserName(SpringSecurityUtil.getUsername()).getId());
        List<Expense> ExpenceList = new ArrayList<Expense>();


        List<Object[]> rows = queryGetExpense.list();

        for(Object[] row : rows) {


            Expense expense = new Expense();

            expense.setId       (Integer.parseInt(row[0].toString()));
            expense.setComment  (row[1].toString());
            expense.setTitle(row[2].toString());
            expense.setCost     (Integer.parseInt(row[3].toString()));
            expense.setCurrency (row[4].toString());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            try {
                expense.setStartDate (df.parse(row[5].toString()));
                expense.setEndDate   (df.parse(row[6].toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            expense.setRepeat(new Repeat(RepeatType.values()[(Integer) row[7]]));
            expense.setUser_id(Integer.parseInt(row[8].toString()));

            ExpenceList.add(expense);
        }

        return ExpenceList;
    }


}
