package ua.dp.levelup.wimm.server.dao;

import ua.dp.levelup.wimm.server.model.ExpenseNotification;
import ua.dp.levelup.wimm.server.model.Payment;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Evgen on 20.10.2015.
 */
    public interface PaymentDao {

        public List<Payment> getPayment(Integer Expense_id,Calendar calendar);


}
