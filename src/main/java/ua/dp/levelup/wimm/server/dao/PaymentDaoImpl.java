package ua.dp.levelup.wimm.server.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.wimm.server.model.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by Evgen on 20.10.2015.
 */
@Repository
@Transactional
public class PaymentDaoImpl implements PaymentDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Payment> getPayment(Integer Expense_id,Calendar calendar){

        Session session = sessionFactory.getCurrentSession();

        Query queryGetPayment = (Query) session.createQuery(
                "FROM Payment " +
                "WHERE expense_id=:Expence_id " +
                "AND date > :date_SP ");

        queryGetPayment.setCalendarDate("date_SP", calendar);
        ArrayList<Payment> ListPayment = (ArrayList<Payment>) queryGetPayment.setParameter("Expence_id", Expense_id).list();

        return ListPayment;
    }

}
