package ua.dp.levelup.wimm.server.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dan on 30.08.2015.
 */
@Entity
@Table
public class Expense {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column
    private String image;

    @Column
    private String title;

    @Column
    private String comment;

    @Column
    private Integer cost;

    @Column
    private String currency;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "expense_id")
    private List<Payment> history = new ArrayList<Payment>();

    @Column
    private Integer user_id;

    @Column
    private Date startDate;

    @Column
    private Date endDate;

    @Embedded
    private Repeat repeat;

    public Expense() {
    }

    public Expense(String image, String title, String comment, Integer cost, String currency,
                   List<Payment> history, Repeat repeat, Integer user_id) {
        this.image = image;
        this.title = title;
        this.comment = comment;
        this.cost = cost;
        this.currency = currency;
        this.history = history;
        this.repeat = repeat;
        this.user_id = user_id;

    }

    public Expense(String image, String title, String comment, Integer cost, String currency,
                   List<Payment> history, Integer user_id, Date startDate, Date endDate, Repeat repeat) {
        this.image = image;
        this.title = title;
        this.comment = comment;
        this.cost = cost;
        this.currency = currency;
        this.history = history;
        this.user_id = user_id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.repeat = repeat;
    }






    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<Payment> getHistory() {
        return history;
    }

    public void setHistory(List<Payment> history) {
        this.history = history;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Repeat getRepeat() {
        return repeat;
    }

    public void setRepeat(Repeat repeat) {
        this.repeat = repeat;
    }
}