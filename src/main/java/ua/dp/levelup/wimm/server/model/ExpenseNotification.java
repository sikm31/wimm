package ua.dp.levelup.wimm.server.model;

/**
 * Created by Evgen on 20.10.2015.
 */
public class ExpenseNotification {

    private String title;
    private int expensePlan;
    private int expenseFact;
    private String comment;

    private ExpenseNotification(Builder builder){
        title=builder.title;
        expensePlan=builder.expensePlan;
        expenseFact=builder.expenseFact;
        comment=builder.comment;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getExpensePlan() {
        return expensePlan;
    }

    public void setExpensePlan(int expensePlan) {
        this.expensePlan = expensePlan;
    }

    public int getExpenseFact() {
        return expenseFact;
    }

    public void setExpenseFact(int expenseFact) {
        this.expenseFact = expenseFact;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }



    public static class Builder{

        private String title;
        private int expensePlan;
        private int expenseFact;
        private String comment;

        public Builder(){}

        public  Builder setTitle(String val){
            title=val;
            return this;
        }
        public  Builder setExpensePlan(int val){
            expensePlan=val;
            return this;
        }
        public  Builder setExpenseFact(int val) {
            expenseFact = val;
            return this;
        }
        public  Builder setComment(String val){
            comment=val;
            return this;
        }

        public  ExpenseNotification build(){
            return  new ExpenseNotification(this);
        }

    }



}
