package ua.dp.levelup.wimm.server.model;

import javax.persistence.*;

/**
 * Created by y.voytovich on 02.09.2015.
 */

@Embeddable
public class Repeat {


    @Column
    @Enumerated
    private RepeatType type;

    /**
     * Should be a '1,5,9'
     * or '1'
     */
    @Column
    private Integer day;

    public Repeat() {
    }

    public Repeat(RepeatType type) {
        this();
        this.type = type;
    }

    public Repeat(RepeatType type, Integer day) {
        this.type = type;
        this.day = day;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public RepeatType getType() {
        return type;
    }

    public void setType(RepeatType type) {
        this.type = type;
    }
}
