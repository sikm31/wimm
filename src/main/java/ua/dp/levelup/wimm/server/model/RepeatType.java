package ua.dp.levelup.wimm.server.model;

import java.util.Date;

/**
 * Created by dan on 18.10.2015.
 */
public enum RepeatType {

    EVERY_MONTH, EVERY_WEEK, EVERY_DAY, SINGLE;

    public static RepeatType valueOf(int number) {
        return RepeatType.values()[number];
    }

}
