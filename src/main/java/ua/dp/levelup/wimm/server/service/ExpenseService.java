package ua.dp.levelup.wimm.server.service;

import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.model.ExpenseNotification;
import ua.dp.levelup.wimm.server.model.Payment;

import java.util.Date;
import java.util.List;

/**
 * Created by yuri on 29.08.2015.
 */
public interface ExpenseService {
    public void addExpense(Expense expense);
    public void deleteExpense(Integer id);
    public void editExpense(Expense expense);
    public List<Expense> getAll();
    public Expense getExpense(Integer id);
    public List<Expense> getAllForPeriod(Date startDate, Date endDate);
    public List<Expense> getAllExpenseWithoutPayment();
    public List<Expense> getExpenseAndPaymentForPeriod(Date startPoint, Date endPoint);
}
