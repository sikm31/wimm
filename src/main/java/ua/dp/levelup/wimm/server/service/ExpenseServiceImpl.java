package ua.dp.levelup.wimm.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.wimm.server.dao.ExpenseDao;
import ua.dp.levelup.wimm.server.dao.PaymentDao;
import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.model.ExpenseNotification;
import ua.dp.levelup.wimm.server.model.Payment;
import ua.dp.levelup.wimm.server.model.Repeat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by yuri on 29.08.2015.
 */
@Service
public class ExpenseServiceImpl implements ExpenseService {

    @Autowired
    ExpenseDao expenseDao;

    @Override
    public void addExpense(Expense expense) {
        expenseDao.addExpense(expense);
    }

    @Override
    public void deleteExpense(Integer id) {
        expenseDao.deleteExpense(id);
    }

    @Override
    public void editExpense(Expense expense) {
        expenseDao.editExpense(expense);
    }

    @Override
    public List<Expense> getAll() {
        return expenseDao.getAll();
    }

    @Override
    public Expense getExpense(Integer id) {
        return expenseDao.getExpense(id);
    }

    @Override
    public List<Expense> getAllForPeriod(Date startDate, Date endDate) {
        return expenseDao.getAllForPeriod(startDate, endDate);
    }

    @Override
    public List<Expense> getExpenseAndPaymentForPeriod(Date startPoint, Date endPoint) {
        return expenseDao.getExpenseAndPaymentForPeriod(startPoint, endPoint);
    }

    @Override
    public List<Expense> getAllExpenseWithoutPayment(){
        return expenseDao.getAllExpenseWithoutPayment();
    }


}