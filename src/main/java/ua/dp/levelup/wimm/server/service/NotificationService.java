package ua.dp.levelup.wimm.server.service;
import ua.dp.levelup.wimm.server.model.ExpenseNotification;

import java.util.List;

/**
 * Created by Evgen on 20.12.2015.
 */

public interface NotificationService{
    public List<ExpenseNotification> getNotification();
}
