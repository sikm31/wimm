package ua.dp.levelup.wimm.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.wimm.server.dao.PaymentDao;
import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.model.ExpenseNotification;
import ua.dp.levelup.wimm.server.model.Payment;
import ua.dp.levelup.wimm.server.model.Repeat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Evgen on 20.12.2015.
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private ExpenseService expenseService;

    @Autowired
    private PaymentService paymentService;

    @Override
    public List<ExpenseNotification> getNotification() {

        List<Expense> expenseList =  expenseService.getAllExpenseWithoutPayment();
        List<ExpenseNotification> listNotification = new ArrayList<ExpenseNotification>();

        for ( Expense currentExpense:expenseList) {

            int Expense_id = currentExpense.getId();

            Repeat repeatOfExpense = currentExpense.getRepeat();
            Calendar calendar = getStartDataOfExpenceRepeat(repeatOfExpense);
            List<Payment> listPayment = paymentService.getPayment(Expense_id, calendar);

            int expenseNotificationPlan = currentExpense.getCost();
            int expenseNotificationFact = getSummOfAllPaymentFromList(listPayment);

            //comment
            if (expenseNotificationPlan < expenseNotificationFact) {

                String expenseNotificationComment = "Вы превысили лимит за заданный период на " +
                                                    (expenseNotificationFact - expenseNotificationPlan)+
                                                    " "+currentExpense.getCurrency() ;

                listNotification.add(
                        new ExpenseNotification
                                .Builder()
                                .setComment     (currentExpense.getTitle()  )
                                .setExpensePlan (expenseNotificationPlan    )
                                .setExpenseFact (expenseNotificationFact    )
                                .setComment     (expenseNotificationComment )
                                .build()
                );
            }
            listPayment.clear();
        }
        return listNotification;
    }


    private int getSummOfAllPaymentFromList(List<Payment> listPayment) {
        int paymentSumm=0;
        for (int j=0;j < listPayment.size();j++) {
            paymentSumm += listPayment.get(j).getValue();
        }
        return paymentSumm;
    }

    private Calendar getStartDataOfExpenceRepeat(Repeat repeatOfExpense){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        switch (repeatOfExpense.getType()) {

            case EVERY_MONTH: {
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                break;
            }
            case EVERY_WEEK: {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                break;
            }
            case EVERY_DAY: {
                break;
            }
            case SINGLE: {
                calendar.set(Calendar.YEAR, 1970);
                break;
            }
        }
        return  calendar;
    }



}
