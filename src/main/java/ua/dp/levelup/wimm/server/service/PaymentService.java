package ua.dp.levelup.wimm.server.service;


import ua.dp.levelup.wimm.server.model.Payment;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Evgen on 20.12.2015.
 */
public interface PaymentService {
    public List<Payment> getPayment(Integer Expense_id, Calendar calendar);
}
