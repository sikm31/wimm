package ua.dp.levelup.wimm.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.wimm.server.dao.PaymentDao;
import ua.dp.levelup.wimm.server.model.Payment;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Evgen on 20.12.2015.
 */
@Service
public class PaymentServiceImpl implements PaymentService {

        @Autowired
        private PaymentDao paymentDao;

       @Override
        public List<Payment> getPayment(Integer Expense_id,Calendar calendar){
            return paymentDao.getPayment(Expense_id,calendar );
        }
}






