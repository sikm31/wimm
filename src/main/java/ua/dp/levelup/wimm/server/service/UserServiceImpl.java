package ua.dp.levelup.wimm.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.wimm.server.dao.UserDao;
import ua.dp.levelup.wimm.server.model.User;

import java.util.List;

/**
 * Created by y.voytovich on 16.09.2015.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userDao.deleteUser(id);
    }

    @Override
    public void editUser(User user) {
            userDao.editUser(user);
    }

    @Override
    public List<User> getAll() {

        return userDao.getAll();
    }

    @Override
    public User getUserById(Integer id) {

        return userDao.getUserById(id);
    }

    @Override
    public User getUserByUserName(String userName) {

        return userDao.getUserByUserName(userName);
    }
}
