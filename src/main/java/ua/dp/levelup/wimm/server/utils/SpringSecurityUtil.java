package ua.dp.levelup.wimm.server.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;


/**
 * Created by e.krotov@hotmail.com on 20.09.2015.
 */
public final class SpringSecurityUtil {

    public static String getUsername() {
//        return ((User)SecurityContextHolder.getContext()
//                .getAuthentication().getPrincipal()).getUsername();
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}
