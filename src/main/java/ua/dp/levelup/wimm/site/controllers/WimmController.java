package ua.dp.levelup.wimm.site.controllers;

import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by dan on 29.08.2015.
 */
@Controller
@RequestMapping("/")
public class WimmController {

    @RequestMapping(method = RequestMethod.GET)
    public String getWimmPage() {
        return "redirect:/index.html";
    }

//todo delete later!!!!!!
    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

//    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
//    public ModelAndView adminPage() {
//
//        ModelAndView model = new ModelAndView();
//        model.addObject("title", "Spring Security Remember Me");
//        model.addObject("message", "This page is for ROLE_ADMIN only!");
//        model.setViewName("admin");
//
//        return model;
//
//    }
//
//    /**
//     * This update page is for user login with password only.
//     * If user is login via remember me cookie, send login to ask for password again.
//     * To avoid stolen remember me cookie to update info
//     */
//    @RequestMapping(value = "/admin/update**", method = RequestMethod.GET)
//    public ModelAndView updatePage(HttpServletRequest request) {
//
//        ModelAndView model = new ModelAndView();
//
//        if (isRememberMeAuthenticated()) {
//            //send login for update
//            setRememberMeTargetUrlToSession(request);
//            model.addObject("loginUpdate", true);
//            model.setViewName("/login");
//
//        } else {
//            model.setViewName("update");
//        }
//
//        return model;
//
//    }
//
//    /**
//     * both "normal login" and "login for update" shared this form.
//     *
//     */
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
//                              @RequestParam(value = "logout", required = false) String logout,
//                              HttpServletRequest request) {
//
//        ModelAndView model = new ModelAndView();
//        if (error != null) {
//            model.addObject("error", "Invalid username and password!");
//
//            //login form for update page
//            //if login error, get the targetUrl from session again.
//            String targetUrl = getRememberMeTargetUrlFromSession(request);
//            System.out.println(targetUrl);
//            if(StringUtils.hasText(targetUrl)){
//                model.addObject("targetUrl", targetUrl);
//                model.addObject("loginUpdate", true);
//            }
//
//        }
//
//        if (logout != null) {
//            model.addObject("msg", "You've been logged out successfully.");
//        }
//        model.setViewName("login");
//
//        return model;
////        return "redirect:/index.html";
//    }
//
//    /**
//     * Check if user is login by remember me cookie, refer
//     * org.springframework.security.authentication.AuthenticationTrustResolverImpl
//     */
//    private boolean isRememberMeAuthenticated() {
//
//        Authentication authentication =
//                SecurityContextHolder.getContext().getAuthentication();
//        if (authentication == null) {
//            return false;
//        }
//
//        return RememberMeAuthenticationToken.class.isAssignableFrom(authentication.getClass());
//    }
//
//    /**
//     * save targetURL in session
//     */
//    private void setRememberMeTargetUrlToSession(HttpServletRequest request){
//        HttpSession session = request.getSession(false);
//        if(session!=null){
//            session.setAttribute("targetUrl", "/admin/update");
//        }
//    }
//
//    /**
//     * get targetURL from session
//     */
//    private String getRememberMeTargetUrlFromSession(HttpServletRequest request){
//        String targetUrl = "";
//        HttpSession session = request.getSession(false);
//        if(session!=null){
//            targetUrl = session.getAttribute("targetUrl")==null?""
//                    :session.getAttribute("targetUrl").toString();
//        }
//        return targetUrl;
//    }



}
