<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Admin</title>
<head>
    <link rel="stylesheet" href="css/main.css" type="text/css"/>
    <script type="application/javascript" src="js/jquery.js"></script>
    <script type="application/javascript" src="js/main.js"></script>
</head>
</head>
<body>
<c:url var="editexpense" value="/api/admin/expensemanage"></c:url>
<c:url var="edituser" value="/api/admin/usermanage"></c:url>
Редактирование платежа <a href="${editexpense}">Выбор</a>
</br>
Полный список пользователей и редактирование <a href="${edituser}">Выбор</a>

</body>
</html>
