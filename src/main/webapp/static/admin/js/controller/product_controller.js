'use strict';

App.controller('ProductRestController', ['$scope', 'ProductService', function($scope, ProductService) {
    var self = this;
    self.product={name:'',image:'',article:'',cost:'',shortdescriprion:'',fulldescription:''};
    self.product=[];

    self.getAllProduct = function(){
        ProductService.getAllProduct()
            .then(
            function(d) {
                self.product = d;
            },
            function(errResponse){
                console.error('Error while fetching product');
            }
        );
    };

    self.addProduct = function(product){
        ProductService.addProduct(product)
            .then(
            self.getAllProduct,
            function(errResponse){
                console.error('Error while creating Product.');
            }
        );
    };

    self.editProduct = function(product, id){
        ProductService.editProduct(product, id)
            .then(
            self.getAllProduct,
            function(errResponse){
                console.error('Error while updating Product.');
            }
        );
    };

    self.deleteProduct = function(id){
        ProductService.deleteProduct(id)
            .then(
            self.getAllProduct,
            function(errResponse){
                console.error('Error while deleting User.');
            }
        );
    };

    self.getAllProduct();

    self.submit = function() {
        if(self.product.id==null){
            console.log('Saving New Product', self.product);
            self.addProduct(self.product);
        }else{
            self.editProduct(self.product, self.product.id);
            console.log('product updated with id ', self.product.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.product.length; i++){
            if(self.product[i].id == id) {
                self.product = angular.copy(self.product[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        for(var i = 0; i < self.product.length; i++){
            if(self.product[i].id == id) {
                self.reset();
                break;
            }
        }
        self.deleteProduct(id);
    };


    self.reset = function(){
        self.product={name:'',image:'',article:'',cost:'',shortdescriprion:'',fulldescription:''};
        $scope.myForm.$setPristine(); //reset Form
    };

}]);
