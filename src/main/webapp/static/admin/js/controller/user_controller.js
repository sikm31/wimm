'use strict';

App.controller('UserController', ['$scope', 'UserService', function ($scope, UserService) {
    var self = this;
    self.user = {username: '', password: '', enabled: '', role: ''};
    self.users = [];
    self.listAllUsers = listAllUsers;
    self.createUser = createUser;
    self.createUser = deleteUser;
    listAllUsers();

    function listAllUsers() {
        UserService.listAllUsers()
            .then(
            function (d) {
                self.users = d;
            },
            function (errResponse) {
                console.error('Error while fetching Currencies');
            }
        );
    };

    function createUser(user) {
        UserService.createUser(user)
            .then(
            listAllUsers(),
            function (errResponse) {
                console.error('Error while creating User.');
            }
        );
    };

    self.updateUser = function (user, id) {
        UserService.updateUser(user, id)
            .then(
            listAllUsers(),
            function (errResponse) {
                console.error('Error while updating User.');
            }
        );
    };

    function deleteUser(id) {
        UserService.deleteUser(id)
            .then(
            listAllUsers(),
            function (errResponse) {
                console.error('Error while deleting User.');
            }
        );
    };



    self.submit = function () {
        if (self.user.id == null) {
            console.log('Saving New User', self.user);
            createUser(self.user);
        } else {
            self.updateUser(self.user, self.user.id);
            console.log('User updated with id ', self.user.id);
        }
        self.reset();
    };

    self.edit = function (id) {
        console.log('id to be edited', id);
        for (var i = 0; i < self.users.length; i++) {
            if (self.users[i].id == id) {
                self.user = angular.copy(self.users[i]);
                break;
            }
        }
    };

    self.remove = function (id) {
        console.log('id to be deleted', id);
        for (var i = 0; i < self.users.length; i++) {
            if (self.users[i].id == id) {
                self.reset();
                break;
            }
        }
        deleteUser(id);
    };


    self.reset = function () {
        self.user = {username: '', password: '', enabled: '', role: ''};
        $scope.myForm.$setPristine(); //reset Form
    };

}]);
