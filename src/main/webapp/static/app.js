(function () {

    'use strict';
// Declare app level module which depends on views, and components
    angular
        .module('myApp', [
            'ngRoute',
            'ngResource',
            'ngMaterial',
            'pages.main',
            'pages.settings',
            'pages.about',
            'myApp.version'
        ])
        .config(['$routeProvider', '$mdThemingProvider', '$mdIconProvider',
            function ($routeProvider, $mdThemingProvider, $mdIconProvider) {

                $routeProvider.otherwise({redirectTo: '/main'});

                $mdThemingProvider.theme('default')
                    .primaryPalette('green')
                    .accentPalette('red');

                $mdIconProvider.icon("menu", "./svg/menu.svg", 24)

            }
        ])
        .controller('PageController', [
            '$mdSidenav', '$mdBottomSheet', '$log', '$q', '$location', '$resource',
            PageController
        ]);

    function PageController($mdSidenav, $mdBottomSheet, $log, $q, $location, $resource) {
        var self = this;

        $resource('/login')
        self.selected = null;
        self.toggleSidenav = toggleSidenav;
        self.go = go;

        function toggleSidenav() {
            var pending = $mdBottomSheet.hide() || $q.when(true);
            pending.then(function () {
                $mdSidenav('left').toggle();
            });
        }

        function go(url) {
            $location.path(url);
            self.toggleSidenav();
        }
    }
})();
