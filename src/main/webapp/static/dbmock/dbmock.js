'use strict';

angular.module('charges', ['ngRoute'])

.service('ChargesService', ['$q', '$resource', function($q, $resource) {
//      var charges = [{
//        id: 0,
//        image: 'http://placehold.it/350x150',
//        title: 'Home',
//        comment: 'asdfb sadlfk;lop dsf,dledfl',
//        cost: '321',
//        costType: 'UAH',
//        history: [
//          {value: 5}, {value:100}
//        ],
//        repeat: {
//          type: 'single'
//        }
//      }, {
//        id: 1,
//        image: 'http://placehold.it/350x150',
//        title: 'Food',
//        comment: 'asdfb sadlfk;lop dsf,dledfl',
//        cost: '321',
//        costType: 'USD',
//        repeat: {
//          type: 'every-day'
//        }
//      }, {
//        id: 2,
//        image: 'http://placehold.it/350x150',
//        title: 'asdf',
//        comment: 'asdfb sadlfk;lop dsf,dledfl',
//        cost: '321',
//        costType: 'USD',
//        repeat: {
//          type: 'every-week'
//        }
//      }, {
//        id: 3,
//        image: 'http://placehold.it/350x150',
//        title: 'asdf',
//        comment: 'asdfb sadlfk;lop dsf,dledfl',
//        cost: '321',
//        costType: 'USD',
//        repeat: {
//          type: 'every-month'
//        }
//      }, {
//        id: 4,
//        image: 'http://placehold.it/350x150',
//        title: 'asdf',
//        comment: 'asdfb sadlfk;lop dsf,dledfl',
//        cost: '321',
//        costType: 'USD',
//        repeat: {
//          type: 'every-year'
//        }
//      }, {
//        id: 5,
//        image: 'http://placehold.it/350x150',
//        title: 'asdf',
//        comment: 'asdfb sadlfk;lop dsf,dledfl',
//        cost: '321',
//        costType: 'UAH',
//        repeat: {
//          type: 'custom'
//        }
//      }];

      return {
        loadCharges: function () {
          // Simulate async nature of real remote calls
            var payment = $resource('/api/payment');
            return $q.when(payment.query().$promise);
        }
      }
}]);