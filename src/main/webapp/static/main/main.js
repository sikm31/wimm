'use strict';

angular.module('pages.main', ['ngRoute', 'charges'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/main', {
            templateUrl: 'main/main.html',
            controller: 'MainCtrl'
        });
    }])

    .controller('MainCtrl', ['ChargesService', '$mdDialog', function (ChargesService, $mdDialog) {
        var self = this;

        self.title = "WIMM";
        self.charges = [];
        self.addCharge = addCharge;

        ChargesService
            .loadCharges()
            .then( function (charges) {
                self.charges = [].concat(charges);
            });

        function addCharge() {
            self.charges.push({
                id: self.charges[self.charges.length - 1].id + 1,
                image: '',
                title: '',
                comment: '',
                cost: '',
                repeat: {
                    type: 'single'
                }
            });
        }

        self.showPopup = function (ev) {
            var popup = $mdDialog.confirm()
                .title('Would you like to delete your debt?')
                .content('All of the banks have agreed to forgive you your debts.')
                .ariaLabel('Lucky day')
                .ok('Please do it!')
                .cancel('Sounds like a scam')
                .targetEvent(ev);
            $mdDialog.show(
                popup
            ).then(function() {
                    self.status = 'You decided to get rid of your debt.';
                }, function() {
                    self.status = 'You decided to keep your debt.';
                });
        };
    }]);