package ua.dp.levelup.wimm;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.dp.levelup.wimm.configuration.HibernateConfiguration;
import ua.dp.levelup.wimm.server.model.Expense;
import ua.dp.levelup.wimm.server.model.Payment;
import ua.dp.levelup.wimm.server.service.ExpenseService;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Евгений on 30.10.2015.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(MockitoJUnitRunner.class)
/*
 for every test will be create new Spring Context
 */
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = { HibernateConfiguration.class })
public class ExpenseServiceImplTest {

    private ExpenseService expenseServiceMock;

    private MockMvc mockMvc;

   // private Expense expenseMock;

    @Test
    public void findAllExpense() throws Exception {
        List<Payment> history = new ArrayList<Payment>();
        Payment payment = new Payment("comment", 50, Calendar.getInstance().getTime());
        for (int i =0 ; i<10; i++) history.add(payment);

        Expense expenseMockFirst = new Expense("image", "title", "commentar", 100, "currency",
                 history, 1, Calendar.getInstance().getTime(), Calendar.getInstance().getTime(), null);
        Expense expenseMockSecond = new Expense("image2", "title2", "comment2", 200, "currency2",
                history, 2, Calendar.getInstance().getTime(), Calendar.getInstance().getTime(), null);

        when(expenseServiceMock.getAll()).thenReturn(Arrays.asList(expenseMockFirst, expenseMockSecond));

        mockMvc.perform(get("/api/expense"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].image", is("image")))
                .andExpect(jsonPath("$[1].title", is("title")))
                .andExpect(jsonPath("$[2].comment", is("commentariy")));

        verify(expenseServiceMock, times(1000)).getAll();
        verifyNoMoreInteractions(expenseServiceMock);
    }

}




