package ua.dp.levelup.wimm;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.dp.levelup.wimm.configuration.HibernateConfigurationTest;
import ua.dp.levelup.wimm.server.dao.UserDao;
import ua.dp.levelup.wimm.server.model.User;
import ua.dp.levelup.wimm.server.service.UserService;
import ua.dp.levelup.wimm.server.service.UserServiceImpl;

import static org.mockito.Matchers.any;

/**
 * Created by y.voytovich on 17.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDaoImplTest{

    @Autowired
    UserDao userDao;

    @Autowired
    HibernateConfigurationTest hibernateConfiguration;

    @Autowired
    UserService userService;


    @Before
    public void setup() {
        userService.addUser(new User("evgen","123",true,"ROLE_ADMIN"));
        userService.addUser(new User("yuri","123",true,"ROLE_ADMIN"));
        userService.addUser(new User("dan","123",true,"ROLE_ADMIN"));
        userService.addUser(new User("mkyoung","123",true,"ROLE_USER"));

    }




//    public User getSampleUser(){
//        User u1 = new User();
//        u1.setId(1);
//        u1.setUsername("yuri");
//        u1.setPassword("123");
//        u1.setEnabled(true);
//        u1.setRole("ROLE_ADMIN");
//        return u1;
//    }
    @Test
    public void checkUser() {
        User user = new User();
        Assert.assertNotNull(user.getId());
    }

    @Test
    public void getUserById(){
        Assert.assertNotNull(userDao.getUserById(10));
       // Assert.assertNull(userDao.getUserById(3));
    }


//    @Test
//    public void addUser(){
//        doNothing().when(userDao).addUser(any(User.class));
//        userService.addUser(any(User.class));
//        verify(userDao, atLeastOnce()).addUser(any(User.class));
//    }
//
//    @Test
//    public void editUser(){
//        doNothing().when(userDao).editUser(any(User.class));
//        userService.editUser(any(User.class));
//        verify(userDao, atLeastOnce()).editUser(any(User.class));
//    }
//    @Test
//    public void deleteUser(){
//        doNothing().when(userDao).deleteUser(anyInt());
//        userService.deleteUser(anyInt());
//        verify(userDao, atLeastOnce()).deleteUser(anyInt());
//
//    }
//    @Test
//    public void getAllUser(){
//        when(userDao.getAll()).thenReturn(users);
//        Assert.assertEquals(userService.getAll(),users);
//    }



}
