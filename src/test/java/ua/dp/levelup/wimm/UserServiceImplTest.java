package ua.dp.levelup.wimm;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ua.dp.levelup.wimm.configuration.HibernateConfigurationTest;
import ua.dp.levelup.wimm.server.dao.UserDao;
import ua.dp.levelup.wimm.server.model.User;
import ua.dp.levelup.wimm.server.service.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Created by y.voytovich on 16.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { HibernateConfigurationTest.class })
public class UserServiceImplTest {

    @Mock
    UserDao userDao;

    @InjectMocks
    UserServiceImpl userService;

    @Spy
    List<User> users = new ArrayList<User>();

    public List<User> getUserList(){
        User e1 = new User();
        e1.setId(1);
        e1.setUsername("yuri");
        e1.setPassword("123");
        e1.setEnabled(true);
        e1.setRole("ROLE_ADMIN");

        User e2= new User();
        e2.setId(2);
        e2.setUsername("mkyoung");
        e2.setPassword("123");
        e2.setEnabled(true);
        e2.setRole("ROLE_USER");

        users.add(e1);
        users.add(e2);
        return users;
    }

    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        users = getUserList();
    }

    @Test
    public void getUserById(){
        User user = users.get(0);
        when(userDao.getUserById(anyInt())).thenReturn(user);
        Assert.assertEquals(userService.getUserById(user.getId()), user);
    }


}
